# Chinese (Taiwan) translation of gnome-doc-utils
# Copyright (C) 2005-06 Free Software Foundation, Inc.
# Woodman Tuen <wmtuen@gmail.com>, 2005-2006.
#
#
msgid ""
msgstr ""
"Project-Id-Version: yelp-xsl 3.3.1\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/yelp-xsl/issues\n"
"POT-Creation-Date: 2018-06-10 17:31+0000\n"
"PO-Revision-Date: 2018-06-13 21:24+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese (Taiwan) <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.8\n"

#. (itstool) path: msg/msgstr
#. Translate to default:RTL if your language should be displayed
#. right-to-left, otherwise translate to default:LTR.  Do *not*
#. translate it to "predefinito:LTR", if it isn't default:LTR
#. or default:RTL it will not work.
#.
#: yelp-xsl.xml.in:34
msgid "default:LTR"
msgstr "default:LTR"

#. (itstool) path: msg/msgstr
#. This is used a simple list item seperator in places where simple
#. inline lists have to be constructed dynamically.  Using the default
#. string of ", ", a list would look like this: A, B, C, D.  Using the
#. nonsensical string " - ", a list would look like this: A - B - C - D.
#.
#. Make sure to include leading or trailing spaces if you want them.
#.
#: yelp-xsl.xml.in:47
#, no-wrap
msgid ", "
msgstr ", "

#. (itstool) path: msg/msgstr
#. This is used as the final separator in an inline list of three or
#. more elements.  The string ", " will be used to separate all but
#. the last pair of elements.  Using these two strings, a list of
#. names would be formatted as "Tom, Dick, and Harry".
#.
#. Make sure to include leading or trailing spaces if you want them.
#.
#: yelp-xsl.xml.in:60
#, no-wrap
msgid ", and "
msgstr ", 和"

#. (itstool) path: msg/msgstr
#. This is used as a list item seperator for inline lists of exactly two
#. elements.  A list of two names would be formatted as "Tom and Dick".
#.
#. Make sure to include leading or trailing spaces if you want them.
#.
#: yelp-xsl.xml.in:71
#, no-wrap
msgid " and "
msgstr " 和 "

#. (itstool) path: msg/msgstr
#. http://www.docbook.org/tdg/en/html/qandaentry.html
#.
#. This is used as a label before questions in a question-and-answer
#. set.  Typically, questions are simply numbered and answers are not
#. labelled at all.  However, DocBook allows document authors to set
#. the labelling style for a qandaset.  If the labelling style is set
#. to 'qanda', this string will be used to label questions.
#.
#: yelp-xsl.xml.in:86
msgid "Q:"
msgstr "Q:"

#. (itstool) path: msg/msgstr
#. http://www.docbook.org/tdg/en/html/qandaentry.html
#.
#. This is used as a label before answers in a question-and-answer
#. set.  Typically, answers are simply numbered and answers are not
#. labelled at all.  However, DocBook allows document authors to set
#. the labelling style for a qandaset.  If the labelling style is set
#. to 'qanda', this string will be used to label answers.
#.
#: yelp-xsl.xml.in:99
msgid "A:"
msgstr "A:"

#. (itstool) path: msg/msgstr
#. Title of the footer containing copyrights, credits, license information,
#. and other stuff about the page.
#.
#: yelp-xsl.xml.in:109
msgid "About"
msgstr "關於"

#. (itstool) path: msg/msgstr
#. Accessible title for an advanced note.
#: yelp-xsl.xml.in:114
msgid "Advanced"
msgstr "進階"

#. (itstool) path: msg/msgstr
#. Default title for a bibliography.
#: yelp-xsl.xml.in:119
msgid "Bibliography"
msgstr "參考書目"

#. (itstool) path: msg/msgstr
#. Accessible title for a note about a software bug.
#: yelp-xsl.xml.in:124
msgid "Bug"
msgstr "程式錯誤"

#. (itstool) path: msg/msgstr
#. Revision status of a document or page. Content has been written and
#. reviewed, and it awaiting a final approval.
#.
#: yelp-xsl.xml.in:132
msgid "Candidate"
msgstr "候選"

#. (itstool) path: msg/msgstr
#. Accessible title for a caution note.
#: yelp-xsl.xml.in:137
msgid "Caution"
msgstr "注意"

#. (itstool) path: msg/msgstr
#. Accessible title for an close button.
#: yelp-xsl.xml.in:142
msgid "Close"
msgstr "關閉"

#. (itstool) path: msg/msgstr
#. Default title for a colophon section.
#: yelp-xsl.xml.in:147
msgid "Colophon"
msgstr "版權頁"

#. (itstool) path: msg/msgstr
#. Title for a table of contents for the entire document.
#: yelp-xsl.xml.in:152
msgid "Contents"
msgstr "內容"

#. (itstool) path: msg/msgstr
#. Title for license information when it's a CC license.
#: yelp-xsl.xml.in:157
msgid "Creative Commons"
msgstr "創用 CC"

#. (itstool) path: msg/msgstr
#. Accessible title for a danger note.
#: yelp-xsl.xml.in:162
msgid "Danger"
msgstr "危險"

#. (itstool) path: msg/msgstr
#. Default title for a dedication section.
#: yelp-xsl.xml.in:167
msgid "Dedication"
msgstr "呈獻"

#. (itstool) path: msg/msgstr
#. Revision status of a document or page. Most content has been
#. written, but revisions are still happening.
#.
#: yelp-xsl.xml.in:175
msgid "Draft"
msgstr "草稿"

#. (itstool) path: msg/msgstr
#. Title for a list of editors.
#: yelp-xsl.xml.in:180
msgid "Edited By"
msgstr "編輯者"

#. (itstool) path: msg/msgstr
#. Revision status of a document or page. A senior member of the
#. documentation team has reviewed and approved.
#.
#: yelp-xsl.xml.in:188
msgid "Final"
msgstr "完稿"

#. (itstool) path: msg/msgstr
#. Default title for a glossary.
#: yelp-xsl.xml.in:193
msgid "Glossary"
msgstr "詞彙表"

#. (itstool) path: msg/msgstr
#. Accessible title for an important note.
#: yelp-xsl.xml.in:198
msgid "Important"
msgstr "重要"

#. (itstool) path: msg/msgstr
#. Revision status of a document or page. Work has begun, but
#. not all content has been written.
#.
#: yelp-xsl.xml.in:206
msgid "Incomplete"
msgstr "不完整"

#. (itstool) path: msg/msgstr
#. Default title for an index of terms in a book.
#: yelp-xsl.xml.in:211
msgid "Index"
msgstr "索引"

#. (itstool) path: msg/msgstr
#. Default title for a DocBook legal notice.
#: yelp-xsl.xml.in:216
msgid "Legal"
msgstr "法律聲明"

#. (itstool) path: msg/msgstr
#. Generic title for license information when it's not a known license.
#.
#: yelp-xsl.xml.in:223
msgid "License"
msgstr "授權"

#. (itstool) path: msg/msgstr
#. Title for a list of maintainers.
#: yelp-xsl.xml.in:228
msgid "Maintained By"
msgstr "維護者"

#. (itstool) path: msg/msgstr
#. Automatic heading above a list of guide links.
#: yelp-xsl.xml.in:233
msgid "More Information"
msgstr "更多資訊"

#. (itstool) path: msg/msgstr
#. Link text for a link to the next page in a series.
#: yelp-xsl.xml.in:238
msgid "Next"
msgstr "下一頁"

#. (itstool) path: msg/msgstr
#. Accessible title for a note.
#: yelp-xsl.xml.in:243
msgid "Note"
msgstr "備註"

#. (itstool) path: msg/msgstr
#. Title for a list of links to sections on the current page.
#: yelp-xsl.xml.in:248
msgid "On This Page"
msgstr "這個頁面上"

#. (itstool) path: msg/msgstr
#. Title for a list contributors other than authors, editors, translators,
#. or other types we have specific lists for.
#.
#: yelp-xsl.xml.in:256
msgid "Other Credits"
msgstr "其他致謝人"

#. (itstool) path: msg/msgstr
#. Revision status of a document or page. Content was once current,
#. but needs to be updated to reflect software updates.
#.
#: yelp-xsl.xml.in:264
msgid "Outdated"
msgstr "已過時"

#. (itstool) path: msg/msgstr
#. Accessible title for a package note.
#: yelp-xsl.xml.in:269
msgid "Package"
msgstr "包裝"

#. (itstool) path: msg/msgstr
#. Tooltip on play/pause buttons for audio and video objects.
#: yelp-xsl.xml.in:274
msgid "Pause"
msgstr "暫停"

#. (itstool) path: msg/msgstr
#. Tooltip on play/pause buttons for audio and video objects.
#: yelp-xsl.xml.in:279
msgid "Play"
msgstr "播放"

#. (itstool) path: msg/msgstr
#. Link text for a link to the previous page in a series.
#: yelp-xsl.xml.in:284
msgid "Previous"
msgstr "上一頁"

#. (itstool) path: msg/msgstr
#. Title for a list of publishers.
#: yelp-xsl.xml.in:289
msgid "Published By"
msgstr "發行者"

#. (itstool) path: msg/msgstr
#. Revision status of a document or page. Content has been written
#. and should be reviewed by other team members.
#.
#: yelp-xsl.xml.in:297
msgid "Ready for review"
msgstr "準備好可供檢閱"

#. (itstool) path: msg/msgstr
#. Automatic heading above a list of see-also links.
#: yelp-xsl.xml.in:302
msgid "See Also"
msgstr "另請參閱"

#. (itstool) path: msg/msgstr
#. Figures can automatically scale images down to fit the page width.
#. This is used a tooltip on a link to shrink images back down after
#. they've been expanded to full size.
#.
#: yelp-xsl.xml.in:311
msgid "Scale images down"
msgstr "縮小影像"

#. (itstool) path: msg/msgstr
#. Accessible title for a sidebar note.
#: yelp-xsl.xml.in:316
msgid "Sidebar"
msgstr "側邊欄"

#. (itstool) path: msg/msgstr
#. Revision status of a document or page. No content has been written yet.
#.
#: yelp-xsl.xml.in:323
msgid "Stub"
msgstr "存根"

#. (itstool) path: msg/msgstr
#. Default title for a refsynopsisdiv element. This is the common section
#. title found in most UNIX man pages.
#.
#: yelp-xsl.xml.in:331
msgid "Synopsis"
msgstr "提要"

#. (itstool) path: msg/msgstr
#. Accessible title for a tip.
#: yelp-xsl.xml.in:336
msgid "Tip"
msgstr "提示"

#. (itstool) path: msg/msgstr
#. Title for a list of translators.
#: yelp-xsl.xml.in:341
msgid "Translated By"
msgstr "翻譯者"

#. (itstool) path: msg/msgstr
#. Figures can automatically scale images down to fit the page width.
#. This is used a tooltip on a link to expand images to full size.
#.
#: yelp-xsl.xml.in:349
msgid "View images at normal size"
msgstr "以影像的原始尺寸顯示"

#. (itstool) path: msg/msgstr
#. Accessible title for a warning.
#: yelp-xsl.xml.in:354
msgid "Warning"
msgstr "警告"

#. (itstool) path: msg/msgstr
#. Title for a list of authors.
#: yelp-xsl.xml.in:359
msgid "Written By"
msgstr "撰寫者"

#. (itstool) path: msg/msgstr
#. ID: biblioentry.tooltip
#. This is a format message used to format tooltips on cross references
#. to bibliography entries.
#.
#. Special elements in the message will be replaced with the
#. appropriate content, as follows:
#.
#. <biblioentry.label/> - The term being defined by the glossary entry
#.
#: yelp-xsl.xml.in:375
msgid "View the bibliography entry <biblioentry.label/>."
msgstr "檢視參考書目項目 <biblioentry.label/>。"

#. (itstool) path: msg/msgstr
#. ID: biblioentry.label
#. This is a format message used to format the labels for entries in
#. a bibliography.  The content of the label is often an abbreviation
#. of the authors' names and the year of publication.  In English,
#. these are generally formatted with [square brackets] surrounding
#. them.
#.
#. This string is similar to citation.label, but they are used in
#. different places.  The citation formatter is used when referencing
#. a bibliography entry in running prose.  This formatter is used for
#. the actual bibliography entry.  You may use the same formatting for
#. both, but you don't have to.
#.
#. Special elements in the message will be replaced with the
#. appropriate content, as follows:
#.
#. <biblioentry.label/> - The text content of the bibliography label
#.
#: yelp-xsl.xml.in:398
msgid "[<biblioentry.label/>]"
msgstr "[<biblioentry.label/>]"

#. (itstool) path: msg/msgstr
#. ID: citation.label
#. This is a format message used to format inline citations to other
#. published works.  The content is typically an abbreviation of the
#. authors' last names.  In English, this abbreviation is usually
#. written inside [square brackets].
#.
#. Special elements in the message will be replaced with the
#. appropriate content, as follows:
#.
#. <citation.label/> - The text content of the citation element, possibly
#. as a link to an entry in the bibliography
#.
#: yelp-xsl.xml.in:415
msgid "[<citation.label/>]"
msgstr "[<citation.label/>]"

#. (itstool) path: msg/msgstr
#. ID: comment.name-date
#. This is a format message used to format the citation of a comment
#. made by an editor of a document. This appears on a new line after
#. the title of the comment, if a title is present, or as the first
#. line if a title is not present.
#.
#. This string is used when both the name and the date are supplied.
#. In English, a title together with this format string might produce
#. something like this:
#.
#. Some Comment Title
#. from Shaun McCance on 2010-06-03
#.
#. Here is the text of the comment.
#.
#. If only a name is supplied, and no date, then comment.name is used
#. instead of this string.
#.
#. Special elements in the message will be replaced with the
#. appropriate content, as follows:
#.
#. <comment.name/> - The name of the person making the comment
#. <comment.date/> - The date the comment was made
#.
#: yelp-xsl.xml.in:444
msgid "from <comment.name/> on <comment.date/>"
msgstr "從 <comment.name/> 於 <comment.date/>"

#. (itstool) path: msg/msgstr
#. ID: comment.name
#. This is a format message used to format the citation of a comment
#. made by an editor of a document. This appears on a new line after
#. the title of the comment, if a title is present, or as the first
#. line if a title is not present.
#.
#. This string is used when only the name of the commenter is supplied.
#. In English, a title together with this format string might produce
#. something like this:
#.
#. Some Comment Title
#. from Shaun McCance
#.
#. Here is the text of the comment.
#.
#. If a date is also supplied, then comment.name-date is used instead
#. of this string.
#.
#. Special elements in the message will be replaced with the
#. appropriate content, as follows:
#.
#. <comment.name/> - The name of the person making the comment
#.
#: yelp-xsl.xml.in:472
msgid "from <comment.name/>"
msgstr "從 <comment.name/>"

#. (itstool) path: msg/msgstr
#. ID: copyright.format
#. This is a format message used to format copyright notices. Special
#. elements in the message will be replaced with the appropriate content,
#. as follows:
#.
#. <copyright.years/> - The years the copyrightable material was made
#. <copyright.name/>  - The person or entity that holds the copyright
#.
#: yelp-xsl.xml.in:485
msgid "© <copyright.years/> <copyright.name/>"
msgstr "© <copyright.years/> <copyright.name/>"

#. (itstool) path: msg/msgstr
#. ID: email.tooltip
#. This is a format message used to format tooltips on mailto: links.
#. Special elements in the message will be replaced with the appropriate
#. content, as follows:
#.
#. <string/> - The linked-to email address
#.
#: yelp-xsl.xml.in:497
msgid "Send email to ‘<string/>’."
msgstr "傳送電子郵件至位址「<string/>」。"

#. (itstool) path: msg/msgstr
#. ID: glossentry.tooltip
#. This is a format message used to format tooltips on cross references
#. to glossary entries. Special elements in the message will be replaced
#. with the appropriate content, as follows:
#.
#. <glossterm/> - The term being defined by the glossary entry
#.
#: yelp-xsl.xml.in:509
msgid "Read the definition for ‘<glossterm/>’."
msgstr "讀取「<glossterm/>」定義。"

#. (itstool) path: msg/msgstr
#. ID: glosssee.format
#. This is a format message used to format glossary cross references.
#. This format message controls the sentence used to present the cross
#. reference, not the link itself.  For the formatting of the actual
#. link, see the message glossentry.xref.
#.
#. One or more glosssee elements are presented as a links to the user
#. in the glossary, formatted as a single prose sentence.  For example,
#. if the user is being directed to the entry "foo", the glosssee may
#. be formatted as "See foo."
#.
#. Special elements in the message will be replaced with the
#. appropriate content, as follows:
#.
#. <glosssee/> - The actual link or links of the cross reference
#.
#: yelp-xsl.xml.in:530
msgid "See <glosssee/>."
msgstr "參閱 <glosssee/>。"

#. (itstool) path: msg/msgstr
#. ID: glossseealso.format
#. This is a format message used to format glossary cross references.
#. This format message controls the sentence used to present the cross
#. reference, not the link itself.  For the formatting of the actual
#. link, see the message glossentry.xref.
#.
#. One or more glossseealso elements are presented as a links to the
#. user in the glossary, formatted as a single prose sentence.  For
#. example, if the user is being directed to the entries "foo", "bar",
#. and "baz", the glossseealso may be formatted as "See also foo, bar,
#. baz."
#.
#. Special elements in the message will be replaced with the
#. appropriate content, as follows:
#.
#. <glosssee/> - The actual link or links of the cross reference
#.
#: yelp-xsl.xml.in:552
msgid "See also <glosssee/>."
msgstr "另請參閱 <glosssee/>。"

#. (itstool) path: msg/msgstr
#. ID: quote.format
#. This is a format message used to format inline quotations. Special
#. elements in the message will be replaced with the appropriate content,
#. as follows:
#.
#. <node/> - The text content of the quote element
#.
#: yelp-xsl.xml.in:564
msgid "“<node/>”"
msgstr "“<node/>”"

#. (itstool) path: msg/msgstr
#. ID: quote.format
#. This is a format message used to format inline quotations inside
#. other inline quotations. Special elements in the message will be
#. replaced with the appropriate content, as follows:
#.
#. <node/> - The text content of the quote element
#.
#: yelp-xsl.xml.in:576
msgid "‘<node/>’"
msgstr "‘<node/>’"

#~ msgid "See <seeie/>."
#~ msgstr "參閱 <seeie/>。"

#~ msgid "See also <seeie/>."
#~ msgstr "另請參閱 <seeie/>。"

#~ msgid "yelp-quote-201C.png"
#~ msgstr "yelp-quote-201C.png"
